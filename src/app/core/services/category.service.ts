import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { finalize } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class CategoryService {

  _context$: BehaviorSubject<string[]>;
  get context$(): Observable<string[]> {
    return this._context$.asObservable();
  }

  constructor(
    private httpClient: HttpClient
  ) { 
    this._context$ = new BehaviorSubject<string[]>([]);
  }

  refresh() {
    this.httpClient
      .get<string[]>('https://test-api-72abd.firebaseio.com/Categorie.json')
      .subscribe(data => this._context$.next(data));
  }

  insert(c: string) {
    return this.httpClient
      .put<string[]>('https://test-api-72abd.firebaseio.com/Categorie.json', 
      ['horor', 'sci-fi', 'comedy', c])
      .pipe(finalize(() => this.refresh()));
  }
}
