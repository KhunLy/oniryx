import { Injectable } from '@angular/core';
import { Message } from '../models/message';
import { Observable, BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class MessageService {

  private ws: WebSocket;

  private _context$: BehaviorSubject<Message[]>;

  public get context$(): Observable<Message[]>{
    return this._context$.asObservable();
  }

  get context(): Message[] {
    return this._context$.value;
  }

  canSend$: BehaviorSubject<boolean>;

  constructor() {
    this._context$ = new BehaviorSubject<Message[]>([]);
    this.canSend$ = new BehaviorSubject<boolean>(false);

    this.ws = new WebSocket('ws://localhost/ws/chat');
    this.ws.onopen = () => {
      this.canSend$.next(true);
    }
    this.ws.onmessage = ({data}) => {
      const message: Message = JSON.parse(data);
      const list = this.context;
      list.push(message);
      this._context$.next(list);
    }
  }


  send(m: Message) {    
    this.ws.send(JSON.stringify(m))
  }
}
