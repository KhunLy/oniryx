import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Pokemons } from '../models/pokemons';
import { PokemonDetails } from '../models/pokemon-details';

@Injectable({
  providedIn: 'root'
})
export class PokemonService {
  constructor(
    private httpClient: HttpClient
  ) { }

  getAll(url: string): Observable<Pokemons>{
    return this.httpClient.get<Pokemons>(url);
  }

  getDetails(url: string): Observable<PokemonDetails>{
    return this.httpClient.get<PokemonDetails>(url);
  }
}
