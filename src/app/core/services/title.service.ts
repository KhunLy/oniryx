import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class TitleService {

  private _title$: BehaviorSubject<string>

  set title(v: string) {
    this._title$.next(v);
  }

  get title$(): Observable<string> {
    return this._title$.asObservable();
  }

  constructor() { 
    this._title$ = new BehaviorSubject<string>("default");
  }


}
