import { Injectable } from '@angular/core';
import { LoginModel } from '../models/login.model';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(
    private client: HttpClient
  ) { }

  login(login: LoginModel): Observable<string> {
    return this.client.post<string>(
      environment.petShopApiEndPoint + '/security/login', login
    );
  }
}
