import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SessionService {
  private _token$: BehaviorSubject<string>;

  get token$(): Observable<string> {
    return this._token$.asObservable();
  }

  set token(v: string) {
    localStorage.setItem('TOKEN', v);
    this._token$.next(v);
  }

  get token(): string {
    return this._token$.value; 
  }

  constructor() { 
    console.log(localStorage.getItem('TOKEN'));
    
    this._token$ = new BehaviorSubject<string>(localStorage.getItem('TOKEN'));
  }

  clearSession() {
    localStorage.clear();
    this._token$.next(null);
  }
}
