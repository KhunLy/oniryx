export interface Message {
    author: string;
    content: string;
    date: Date;
}