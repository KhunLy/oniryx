import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor
} from '@angular/common/http';
import { Observable } from 'rxjs';
import { NbDialogService } from '@nebular/theme';
import { finalize } from 'rxjs/operators';
import { LoaderComponent } from 'src/app/shared/components/loader/loader.component';

@Injectable()
export class LoaderInterceptor implements HttpInterceptor {

  constructor(
    private dialogService: NbDialogService
  ) {}

  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
    // let clone = request.clone({setHeaders: {"":""}})
    // return next.handle(clone);
    //afficher un loader
    let ref = this.dialogService.open(LoaderComponent);
    return next.handle(request)
      .pipe(finalize(() => {
          ref.close();
      }));
    //faire disparaitre le loader
  }
}
