import { Injectable } from '@angular/core';
import { PokemonDetails } from '../models/pokemon-details';
import { Resolve, RouterStateSnapshot, ActivatedRouteSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { PokemonService } from '../services/pokemon.service';

@Injectable({
  providedIn: 'root'
})
export class PokemonResolver implements Resolve<PokemonDetails> {

  constructor(
    private pokemonService: PokemonService
  ) { }
  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot)
  : PokemonDetails | Observable<PokemonDetails> | Promise<PokemonDetails> {
    let url = route.params['url'];
    console.log(42,url);
    
    return this.pokemonService.getDetails(url);
  }
}
