import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router, Route, CanDeactivate } from '@angular/router';
import { Observable } from 'rxjs';
import { Demo9Component } from 'src/app/features/demo/components/demo9/demo9.component';
import { NbDialogService } from '@nebular/theme';
import { ConfirmBoxComponent } from 'src/app/shared/components/confirm-box/confirm-box.component';

@Injectable({
  providedIn: 'root'
})
export class ExampleGuard implements CanActivate, CanDeactivate<Demo9Component> {
  constructor(
    private router: Router,
    private dialogService: NbDialogService
  ) {}
  canDeactivate(component: Demo9Component, currentRoute: ActivatedRouteSnapshot, currentState: RouterStateSnapshot, nextState?: RouterStateSnapshot): boolean | UrlTree | Observable<boolean | UrlTree> | Promise<boolean | UrlTree> {
    let ref = this.dialogService.open(ConfirmBoxComponent);
    return ref.onClose;
  }
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    this.router.navigateByUrl('/home');
    return false;
  }
  
}
