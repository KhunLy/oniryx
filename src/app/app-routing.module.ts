import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { AboutComponent } from './about/about.component';
import { LoginComponent } from './login/login.component';
import { IsLoggedGuard } from './core/guards/is-logged.guard';


const routes: Routes = [{
  path: 'home', component: HomeComponent
}, {
  path: 'about', component: AboutComponent
},
{
  path: 'login', component: LoginComponent
},
  { path: 'demo', loadChildren: () => import('./features/demo/demo.module').then(m => m.DemoModule) },
  { path: 'ex', loadChildren: () => import('./features/ex/ex.module').then(m => m.ExModule) },
  { canActivate: [IsLoggedGuard], path: 'pets', loadChildren: () => import('./features/pets/pets.module').then(m => m.PetsModule) }];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
