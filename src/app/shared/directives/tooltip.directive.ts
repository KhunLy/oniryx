import { Directive, Inject, Input, ElementRef, OnInit, HostListener } from '@angular/core';
import { DOCUMENT } from '@angular/common';

@Directive({
  selector: '[appTooltip]'
})
export class TooltipDirective implements OnInit {

  @Input() appTooltip;

  div: any;

  @HostListener("mousemove", ['$event']) onMouseMove($event) {
    console.log($event);
    // this.ref.nativeElement.style.position = "relative";
    this.div.style.position = "absolute";
    this.div.style.left = $event.pageX + 'px';
    this.div.style.top = $event.pageY + 'px';
    this.div.style.display = "block";
  }

  @HostListener("mouseleave") onMouseLeave() {
    this.div.style.display = "none";
  }

  constructor(@Inject(DOCUMENT) private doc, private ref:ElementRef ) { 
    
  }
  ngOnInit(): void {
    this.div = this.doc.createElement('div');
    this.div.innerText = this.appTooltip;
    this.div.style.padding = "10px";
    this.div.style.border = "1px solid black";
    this.div.style.backgroundColor = "#FFF";
    this.div.style.display = "none";
    this.ref.nativeElement.appendChild(this.div);

  }

}
