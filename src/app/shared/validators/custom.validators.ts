import { ValidatorFn, ValidationErrors, AbstractControl, FormGroup } from '@angular/forms';

export class CustomValidators {
    static notBefore(date: Date): ValidatorFn {
        return (control: AbstractControl) => {
            let value = control.value;
            if(value == null) return null;
            if(value > date) return null;
            return { notBefore: true };
        }
    }

    static match(prop1:string, prop2:string): ValidatorFn {
        return (formGroup: FormGroup) => {
            let v1 = formGroup.get(prop1).value;
            let v2 = formGroup.get(prop2).value;
            if(v1 == null || v2 == null) return null;
            if(v1 == v2) return null;
            return { match: true };
        }
    }

    static notBeforeToday(control: AbstractControl): ValidationErrors {
        let value = control.value;
        if(value == null) return null;
        if(value > new Date()) return null;
        return { notBeforeToday: true };
    }
}