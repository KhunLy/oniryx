import { Component, OnInit, Input } from '@angular/core';
import { NbDialogRef } from '@nebular/theme';

@Component({
  selector: 'app-confirm-box',
  templateUrl: './confirm-box.component.html',
  styleUrls: ['./confirm-box.component.scss']
})
export class ConfirmBoxComponent implements OnInit {

  @Input() title: string;

  constructor(private dialogRef: NbDialogRef<ConfirmBoxComponent>) { }

  ngOnInit(): void {
  }

  yes() { this.dialogRef.close(true); }

  no() { this.dialogRef.close(false); }

}
