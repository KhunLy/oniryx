import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-alert',
  templateUrl: './alert.component.html',
  styleUrls: ['./alert.component.scss']
})
export class AlertComponent implements OnInit {

  @Input()
  title: string;

  @Input()
  color: string;

  @Output()
  onClick: EventEmitter<boolean>

  constructor() { 
    this.onClick = new EventEmitter<boolean>();
  }

  ngOnInit(): void {
  }

  yes() { this.onClick.emit(true); }

  no() { this.onClick.emit(false) }

}
