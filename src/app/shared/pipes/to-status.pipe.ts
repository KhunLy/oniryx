import { Pipe, PipeTransform } from '@angular/core';
import { AbstractControl } from '@angular/forms';

@Pipe({
  name: 'toStatus',
  pure: false
})
export class ToStatusPipe implements PipeTransform {

  transform(value: AbstractControl, ...args: unknown[]): string {
    console.log(value.value);
    if(value.pristine && value.untouched)
      return 'info';
    if(value.valid)
      return 'success';
    return 'danger';
  }

}
