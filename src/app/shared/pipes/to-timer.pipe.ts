import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'toTimer'
})
export class ToTimerPipe implements PipeTransform {

  transform(value: number, ...args: unknown[]): string {
    let seconds = (value%60).toString();
    let minutes = Math.floor(value/60).toString();
    return `${minutes.padStart(2, '0')}:${seconds.padStart(2, '0')}`;
  }

}
