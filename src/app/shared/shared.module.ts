import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ToTimerPipe } from './pipes/to-timer.pipe';
import { AlertComponent } from './components/alert/alert.component';
import { NbCardModule, NbButtonModule } from '@nebular/theme';
import { ConfirmBoxComponent } from './components/confirm-box/confirm-box.component';
import { LoaderComponent } from './components/loader/loader.component';
import { ToStatusPipe } from './pipes/to-status.pipe';
import { TooltipDirective } from './directives/tooltip.directive';



@NgModule({
  declarations: [ToTimerPipe, AlertComponent, ConfirmBoxComponent, LoaderComponent, ToStatusPipe, TooltipDirective],
  imports: [
    CommonModule,
    NbCardModule,
    NbButtonModule,
  ],
  exports: [ToTimerPipe, AlertComponent, ToStatusPipe, TooltipDirective]
})
export class SharedModule { }
