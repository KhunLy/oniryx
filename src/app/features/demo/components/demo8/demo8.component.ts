import { Component, OnInit } from '@angular/core';
import { Result } from 'src/app/core/models/pokemons';
import { PokemonService } from 'src/app/core/services/pokemon.service';

@Component({
  selector: 'app-demo8',
  templateUrl: './demo8.component.html',
  styleUrls: ['./demo8.component.scss']
})
export class Demo8Component implements OnInit {

  list: Result[];

  constructor(
    private pokS: PokemonService
  ) { 
    console.log(42);   
  }

  ngOnInit(): void {
    this.pokS.getAll("https://pokeapi.co/api/v2/pokemon")
      .subscribe(data => this.list = data.results);
  }

}
