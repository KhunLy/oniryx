import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, AbstractControl } from '@angular/forms';
import { CustomValidators } from 'src/app/shared/validators/custom.validators';

@Component({
  selector: 'app-demo10',
  templateUrl: './demo10.component.html',
  styleUrls: ['./demo10.component.scss']
})
export class Demo10Component implements OnInit {

  fg: FormGroup;

  constructor() { }

  ngOnInit(): void {
    this.fg = new FormGroup({
      'lastName': new FormControl(null, Validators.compose([
        Validators.required,
        Validators.maxLength(50),
        Validators.pattern('[a-zA-Z]*')
      ])),
      'firstName': new FormControl(null, Validators.required),
      'password': new FormControl(null, Validators.required),
      'confirmPassword': new FormControl(null, Validators.required),
      'birthDate': new FormControl(null, 
        CustomValidators.notBefore(new Date(2000,1,1)))
    }, Validators.compose([
      CustomValidators.match('password', 'confirmPassword')
    ]));
  }

  submit() {
    if(this.fg.valid) {
      console.log(this.fg.value);
    }
  }

  toStatus(value: AbstractControl) {
    if(value.pristine && value.untouched)
      return 'info';
    if(value.valid)
      return 'success';
    return 'danger';
  }

}
