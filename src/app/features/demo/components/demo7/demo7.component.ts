import { Component, OnInit } from '@angular/core';
import { Observable, of, from } from 'rxjs';
import { delay, filter } from 'rxjs/operators';
import { NbDialogService } from '@nebular/theme';
import { ConfirmBoxComponent } from 'src/app/shared/components/confirm-box/confirm-box.component';

@Component({
  selector: 'app-demo7',
  templateUrl: './demo7.component.html',
  styleUrls: ['./demo7.component.scss']
})
export class Demo7Component implements OnInit {

  list$: Observable<string>;
  constructor(private dialogService: NbDialogService) { }

  ngOnInit(): void {
    let list = ['sel', 'poivre', 'sucre'];
    this.list$ = from(list).pipe(delay(5000));
  }

  click() {
     let dRef = this.dialogService.open(ConfirmBoxComponent);
     let ref =  this.list$.pipe(filter(
       data =>  {
         return data.toString().startsWith('s')
      }))
     .subscribe(data => {
      console.log(data);
     }, () => {}, () => {
       dRef.close()
     });
     dRef.onClose.subscribe(data => {
      ref.unsubscribe();
     });
  }

}
