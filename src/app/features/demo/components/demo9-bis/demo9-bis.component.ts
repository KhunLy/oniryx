import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { PokemonService } from 'src/app/core/services/pokemon.service';
import { PokemonDetails } from 'src/app/core/models/pokemon-details';

@Component({
  selector: 'app-demo9-bis',
  templateUrl: './demo9-bis.component.html',
  styleUrls: ['./demo9-bis.component.scss']
})
export class Demo9BisComponent implements OnInit {

  model: PokemonDetails;
  constructor(
    private router: ActivatedRoute
  ) { }

  ngOnInit(): void {
    this.model = this.router.snapshot.data['model'];
  }

}
