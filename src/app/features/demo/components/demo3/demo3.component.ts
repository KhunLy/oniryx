import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-demo3',
  templateUrl: './demo3.component.html',
  styleUrls: ['./demo3.component.scss']
})
export class Demo3Component implements OnInit {

  date: Date;
  nombre: number;
  chaine: string;

  constructor() { }

  ngOnInit(): void {
    this.date = new Date();
    this.nombre = 3.1418;
    this.chaine = 'lorem ipsum';
  }

}
