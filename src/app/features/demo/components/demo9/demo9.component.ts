import { Component, OnInit } from '@angular/core';
import { PokemonService } from 'src/app/core/services/pokemon.service';
import { Pokemons } from 'src/app/core/models/pokemons';

@Component({
  selector: 'app-demo9',
  templateUrl: './demo9.component.html',
  styleUrls: ['./demo9.component.scss']
})
export class Demo9Component implements OnInit {

  model: Pokemons;
  constructor(
    private pokemonService: PokemonService
  ) { }

  ngOnInit(): void {
    this.pokemonService.getAll('https://pokeapi.co/api/v2/pokemon')
      .subscribe(data => this.model = data);
  }

}
