import { Component, OnInit } from '@angular/core';
import { MessageService } from 'src/app/core/services/message.service';

@Component({
  selector: 'app-demo11',
  templateUrl: './demo11.component.html',
  styleUrls: ['./demo11.component.scss']
})
export class Demo11Component implements OnInit {

  author = "Khun";

  constructor(
    public mesService: MessageService
  ) { }

  ngOnInit(): void {  
  }

  send($event) {
    this.mesService.send({
      author: this.author, 
      content: $event.message, 
      date: new Date()
    });
  }

}
