import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-demo4',
  templateUrl: './demo4.component.html',
  styleUrls: ['./demo4.component.scss']
})
export class Demo4Component implements OnInit {

  liste: string[];
  isToggled: boolean;

  constructor() { }

  ngOnInit(): void {
    this.isToggled = false;
    this.liste = ['pommes', 'poires', 'pistaches'];
  }

  toggle() {
    this.isToggled = !this.isToggled;
  }

}
