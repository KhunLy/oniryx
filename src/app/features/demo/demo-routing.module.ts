import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DemoComponent } from './demo.component';
import { Demo1Component } from './components/demo1/demo1.component';
import { Demo2Component } from './components/demo2/demo2.component';
import { Demo3Component } from './components/demo3/demo3.component';
import { Demo4Component } from './components/demo4/demo4.component';
import { Demo5Component } from './components/demo5/demo5.component';
import { Demo6Component } from './components/demo6/demo6.component';
import { Demo7Component } from './components/demo7/demo7.component';
import { Demo8Component } from './components/demo8/demo8.component';
import { Demo9Component } from './components/demo9/demo9.component';
import { Demo9BisComponent } from './components/demo9-bis/demo9-bis.component';
import { PokemonResolver } from 'src/app/core/resolvers/pokemon.resolver';
import { ExampleGuard } from 'src/app/core/guards/example.guard';
import { Demo10Component } from './components/demo10/demo10.component';
import { Demo11Component } from './components/demo11/demo11.component';

const routes: Routes = [{ 
  path: '', 
  component: DemoComponent,
  children: [
    { path: 'demo1', component: Demo1Component },
    { path: 'demo2', component: Demo2Component },
    { path: 'demo3', component: Demo3Component },
    { path: 'demo4', component: Demo4Component },
    { path: 'demo5', component: Demo5Component },
    { path: 'demo6', component: Demo6Component },
    { path: 'demo7', component: Demo7Component },
    { path: 'demo8', component: Demo8Component, canActivate: [ ExampleGuard ] },
    { path: 'demo9', component: Demo9Component,
    canDeactivate: [ ExampleGuard ] },
    { path: 'demo9bis/:url', 
      component: Demo9BisComponent, 
      resolve: { model: PokemonResolver }
     },
    { path: 'demo10', component: Demo10Component },
    { path: 'demo11', component: Demo11Component },

  ]
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DemoRoutingModule { }
