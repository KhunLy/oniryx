import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DemoRoutingModule } from './demo-routing.module';
import { DemoComponent } from './demo.component';
import { Demo1Component } from './components/demo1/demo1.component';
import { Demo2Component } from './components/demo2/demo2.component';
import { NbButtonModule, NbInputModule, NbCardModule, NbIconModule, NbDatepickerModule, NbChatModule } from '@nebular/theme';
import { Demo3Component } from './components/demo3/demo3.component';
import { Demo4Component } from './components/demo4/demo4.component';
import { Demo5Component } from './components/demo5/demo5.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Demo6Component } from './components/demo6/demo6.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { Demo7Component } from './components/demo7/demo7.component';
import { HttpClientModule } from '@angular/common/http';
import { Demo8Component } from './components/demo8/demo8.component';
import { CoreModule } from 'src/app/core/core.module';
import { Demo9Component } from './components/demo9/demo9.component';
import { Demo9BisComponent } from './components/demo9-bis/demo9-bis.component';
import { Demo10Component } from './components/demo10/demo10.component';
import { Demo11Component } from './components/demo11/demo11.component';

@NgModule({
  declarations: [DemoComponent, Demo1Component, Demo2Component, Demo3Component, Demo4Component, Demo5Component, Demo6Component, Demo7Component, Demo8Component, Demo9Component, Demo9BisComponent, Demo10Component, Demo11Component],
  imports: [
    CommonModule,
    DemoRoutingModule,
    NbButtonModule,
    NbInputModule,
    NbCardModule,
    NbIconModule,
    FormsModule,
    SharedModule,
    HttpClientModule,
    CoreModule,
    ReactiveFormsModule,
    NbDatepickerModule.forRoot(),
    NbChatModule.forRoot(),
  ]
})
export class DemoModule { }
