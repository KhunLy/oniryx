import { Component, OnInit, Input } from '@angular/core';
import { PokemonService } from 'src/app/core/services/pokemon.service';
import { PokemonDetails } from 'src/app/core/models/pokemon-details';

@Component({
  selector: 'app-ex3-details',
  templateUrl: './ex3-details.component.html',
  styleUrls: ['./ex3-details.component.scss']
})
export class Ex3DetailsComponent implements OnInit {

  model: PokemonDetails;

  @Input() 
  set url(url:string) {
    if(!url) return;
    this.pokeS.getDetails(url)
      .subscribe(data=>this.model=data);
  }

  constructor(private pokeS: PokemonService) { }

  ngOnInit(): void {
  }

}
