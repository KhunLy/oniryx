import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { PokemonService } from 'src/app/core/services/pokemon.service';
import { Pokemons } from 'src/app/core/models/pokemons';

@Component({
  selector: 'app-ex3-master',
  templateUrl: './ex3-master.component.html',
  styleUrls: ['./ex3-master.component.scss']
})
export class Ex3MasterComponent implements OnInit {

  @Output() changeUrl: EventEmitter<string>;
  model: Pokemons;
  constructor(private pokeS: PokemonService) {
    this.changeUrl = new EventEmitter<string>();
  }

  ngOnInit(): void {
    this.pokeS.getAll("https://pokeapi.co/api/v2/pokemon")
      .subscribe(data => this.model = data);
  }

  prev() {
    this.pokeS.getAll(this.model.previous)
      .subscribe(data => this.model = data);
  }
  next() {
    this.pokeS.getAll(this.model.next)
      .subscribe(data => this.model = data);
  }

  select(url: string) {
    this.changeUrl.emit(url);
  }
}
