import { Component, OnInit, ViewChild, ElementRef, Inject } from '@angular/core';
import { Task } from 'src/app/core/models/task';
import { NbDialogService } from '@nebular/theme';
import { ConfirmBoxComponent } from 'src/app/shared/components/confirm-box/confirm-box.component';

@Component({
  selector: 'app-ex2',
  templateUrl: './ex2.component.html',
  styleUrls: ['./ex2.component.scss']
})
export class Ex2Component implements OnInit {

  @ViewChild("input") 
  input: ElementRef;
  newItem: string;
  items: Task[];

  constructor(
    private dialogService: NbDialogService,
  ) { }

  ngOnInit(): void {
    this.items = [];
  }

  add() {
    this.items.push({
      name: this.newItem,
      isChecked: false
    });
    this.newItem = null;
    this.input.nativeElement.focus();
  }

  remove(index: number) {
    let ref = this.dialogService.open(ConfirmBoxComponent, {
      closeOnBackdropClick: false,
      context: {
        title: this.items[index].name
      }
    });
    ref.onClose.subscribe(data => {
      if(data) this.items.splice(index,1);
    });
  }

  check(elem: Task) {
    elem.isChecked = !elem.isChecked;
  }

}
