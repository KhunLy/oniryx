import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-ex1',
  templateUrl: './ex1.component.html',
  styleUrls: ['./ex1.component.scss']
})
export class Ex1Component implements OnInit {

  nbSeconds: number;

  idTimer: any;

  constructor() { }

  ngOnInit(): void {
    this.nbSeconds = 0;
  }

  start() {
    this.idTimer = setInterval(() => {
      this.nbSeconds++;
    }, 1000);
  }

  stop() {
    clearInterval(this.idTimer);
    this.idTimer = null;
  }

  reset() {
    this.nbSeconds = 0;
    this.stop();
  }

}
