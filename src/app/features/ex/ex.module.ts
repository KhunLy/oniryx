import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ExRoutingModule } from './ex-routing.module';
import { ExComponent } from './ex.component';
import { Ex1Component } from './components/ex1/ex1.component';
import { NbButtonModule, NbCardModule, NbInputModule, NbDialogModule, NbListModule } from '@nebular/theme';
import { SharedModule } from 'src/app/shared/shared.module';
import { Ex2Component } from './components/ex2/ex2.component';
import { FormsModule } from '@angular/forms';
import { Ex3Component } from './components/ex3/ex3.component';
import { Ex3MasterComponent } from './components/ex3/ex3-master/ex3-master.component';
import { Ex3DetailsComponent } from './components/ex3/ex3-details/ex3-details.component';


@NgModule({
  declarations: [ExComponent, Ex1Component, Ex2Component, Ex3Component, Ex3MasterComponent, Ex3DetailsComponent],
  imports: [
    CommonModule,
    ExRoutingModule,
    NbButtonModule,
    NbCardModule,
    SharedModule,
    NbInputModule,
    FormsModule,
    NbDialogModule.forChild(),
    NbListModule,
  ]
})
export class ExModule { }
