import { Component, OnInit, AfterViewInit } from '@angular/core';
import { TitleService } from '../core/services/title.service';
import { CategoryService } from '../core/services/category.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  input: string;

  constructor(
    private catService: CategoryService,
    private titleService: TitleService
  ) { 
    this.titleService.title = 'Home Page';
  }

  ngOnInit(): void {
  }

  click() {
    this.catService.insert(this.input).subscribe();
  }

}
