import { Component, OnInit } from '@angular/core';
import { AuthService } from '../core/services/auth.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { NbToastrService } from '@nebular/theme';
import { Router } from '@angular/router';
import { SessionService } from '../core/services/session.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  fg: FormGroup;
  constructor(
    private authService: AuthService,
    private tostr: NbToastrService,
    private router: Router,
    private session: SessionService
  ) { }

  ngOnInit(): void {
    this.fg = new FormGroup({
      email: new FormControl(null, Validators.compose([
        Validators.required,
        Validators.maxLength(255),
        Validators.email
      ])),
      password: new FormControl(null, Validators.compose([
        Validators.required,
        Validators.maxLength(255)
      ]))
    });
  }

  login() {
    this.authService.login(this.fg.value).subscribe(data => {
      // enregister dans le localstorage le token
      this.session.token = data;
      // afficher un message de success
      this.tostr.success("Welcome ...");
      // rediriger le user vers .. 
      this.router.navigateByUrl('/home');
    }, error => {
      // afficher un message d'erreur
      this.tostr.danger(JSON.stringify(error));
    });
  }



}
