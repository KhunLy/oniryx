import { Component, OnInit } from '@angular/core';
import { NbMenuItem } from '@nebular/theme';
import { PokemonService } from './core/services/pokemon.service';
import { TitleService } from './core/services/title.service';
import { Observable } from 'rxjs';
import { CategoryService } from './core/services/category.service';
import { SessionService } from './core/services/session.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  items: NbMenuItem[];
  title$: Observable<string>;
  constructor(
    private pok: PokemonService,
    public titleService: TitleService,
    private catService: CategoryService,
    public session: SessionService,
    private router: Router,
  ) {
    let w = new Worker('./token.worker', { type: 'module' });
    setInterval(() => {
      if(localStorage.getItem('TOKEN'))
        w.postMessage(localStorage.getItem('TOKEN'));
    }, 10000)
    w.onmessage = ({data}) => {
      session.token = data;
    }
  }

  ngOnInit() {
    this.catService.refresh();
    this.title$ = this.titleService.title$;
    this.items = [
      { title: 'Home', link: '/home', icon: 'home' },
      { title: 'About', link: '/about', icon: 'sun' },
      { title: 'Demo', icon: 'book', children: [
        { title: 'Demo 1 - Binding 1 Way', link: '/demo/demo1' },
        { title: 'Demo 2 - Events', link: '/demo/demo2' },
        { title: 'Demo 3 - Pipe', link: '/demo/demo3' },
        { title: 'Demo 4 - Structural Directives', link: '/demo/demo4' },
        { title: 'Demo 5 - Binding 2 Ways', link: '/demo/demo5' },
        { title: 'Demo 6 - @Input() - @Output()', link: '/demo/demo6' },
        { title: 'Demo 7 - Obervables', link: '/demo/demo7' },
        { title: 'Demo 8 - HttpClient', link: '/demo/demo8' },
        { title: 'Demo 9', link: '/demo/demo9' },
        { title: 'Demo 10', link: '/demo/demo10' },
        { title: 'Demo 11 - WebSocket', link: '/demo/demo11' },
      ] },
      { title: 'Exercices', icon: 'edit', children: [
        { title: 'Ex 1 - Timer', link: '/ex/ex1' },
        { title: 'Ex 2 - Shopping List', link: '/ex/ex2' },
        { title: 'Ex 3 - Pokedex', link: '/ex/ex3' },
      ] }, 
      { title: 'Categories', children: [

      ] }

    ];
    this.catService.context$.subscribe(data => {
      this.items.find(x => x.title === "Categories")
      .children = data.map(x => {return {title: x}})
    })

  }

  logout() {
    this.session.clearSession();
    // on pourrait rediriger
    this.router.navigateByUrl('/login');
  }
}
